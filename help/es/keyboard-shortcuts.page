<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="keyboard-shortcuts" xml:lang="es">

  <info>
    <link type="guide" xref="index#options"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Desactivar el envío de atajos de teclado a la máquina remota.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  </info>

  <title>Atajos de teclado</title>

  <p>Puede activar o desactivar los atajos de teclado pulsando <guiseq><gui style="menu">Ver</gui> <gui style="menuitem">Atajos de teclado</gui></guiseq>.</p>

  <p>Activar esta opción le permite usar atajos de teclado, como <keyseq><key>Ctrl</key><key>N</key></keyseq>, aceleradores de menú y mnemónicos, como <keyseq><key>Alt</key><key>M</key></keyseq>, con el <app>Visor de escritorios remotos</app>. Esto significa que <app>Vinagre</app> interceptará todas esas combinaciones en lugar de enviarlas a la máquina remota. Esta opción está desactivada de forma predeterminada, ya que la mayoría del tiempo estará interactuando con el equipo al que se ha conectado.</p>

  <note>
    <p>Cuando la opción <gui>Atajos de teclado</gui> está desactivada, <keyseq><key>Ctrl</key><key>Alt</key><key>Supr</key></keyseq> es la única combinación de teclas que no se enviará al escritorio remoto. Para enviar esta combinación de teclas, elija <guiseq><gui style="menu">Remoto</gui><gui style="menuitem">Enviar Ctrl-Alt-Supr</gui></guiseq>, o use el botón correspondiente en la barra de herramientas.</p>
  </note>

</page>
