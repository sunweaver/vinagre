<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="el">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Σύνδεση με έναν άλλο υπολογιστή στο τοπικό σας δίκτυο.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bασίλης Τσιβίκης</mal:name>
      <mal:email>vasitsiv.dev@gmail.com</mal:email>
      <mal:years>2011 </mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Δημιουργία σύνδεσης</title>

  <p>Μπορείτε να συνδεθείτε με άλλους υπολογιστές στο τοπικό σας δίκτυο χρησιμοποιώντας το <app>Vinagre</app>.</p>

  <steps>
    <item>
      <p>Επιλέξτε <guiseq><gui style="menu">Απομακρυσμένος</gui> <gui style="menuitem">Σύνδεση</gui></guiseq>.</p>
    </item>
    <item>
      <p>Επιλέξτε το <gui>Πρωτόκολλο</gui> και τον <gui>Κεντρικό υπολογιστή</gui> για τη σύνδεση.</p>
      <note>
        <p>Κάποια πρωτόκολλα επιτρέπουν να δείτε όλους τους διαθέσιμους υπολογιστές στο τοπικό σας δίκτυο πατώντας στο κουπί <gui style="button">Εύρεση</gui>. Χωρίς την υποστήριξη του <app>Avahi</app>, αυτό το κουμπί δεν θα εμφανίζεται.</p>
      </note>
    </item>
    <item>
      <p>Επιλέξτε τις επιλογές που θέλετε ενεργοποιημένες όταν γίνετε η σύνδεση. Οι επιλογές θα διαφέρουν ανάλογα με το πρωτόκολλο που χρησιμοποιείτε.</p>
    </item>
    <item>
      <p>Πατήστε το κουμπί <gui style="button">Σύνδεση</gui>.</p>
      <p>Σε αυτό το σημείο, η απομακρυσμένη επιφάνεια εργασίας μπορεί να χρειαστεί να επιβεβαιώσει την σύνδεση. Αν συμβαίνει αυτό, ο θεατής μπορεί να παραμείνει με μαύρη οθόνη μέχρι να επιβεβαιωθεί η σύνδεση.</p>
      <note>
        <p>Μερικοί υπολογιστές μπορεί να απαιτούν μια ασφαλή σύνδεση: ένας διάλογος πιστοποίησης θα εμφανισθεί, ζητώντας τα διαπιστευτήρια. Ο τύπος τους εξαρτάται από τον απομακρυσμένο κεντρικό υπολογιστή· μπορεί να είναι ένας κωδικός και ένα όνομα χρήστη. Αν επιλέξετε <gui>Υπενθύμιση κωδικού πρόσβασης</gui>, το <app>Vinagre</app> θα αποθηκεύσει τις πληροφορίες χρησιμοποιώντας την <app>κλειδοθήκη GNOME</app>.</p>
      </note>
    </item>
  </steps>

  <p>Αν η σύνδεση χρησιμοποιήθηκε προηγουμένως, μπορείτε επίσης να την προσπελάσετε μέσα από <guiseq><gui style="menu">Απομακρυσμένη</gui> <gui style="menuitem">Πρόσφατες συνδέσεις</gui></guiseq>.</p>

  <p>Για να κλείσετε μια σύνδεση, επιλέξτε <guiseq><gui style="menu">Απομακρυσμένη</gui> <gui style="menuitem">Κλείσιμο</gui></guiseq> ή πατήστε το κουμπί <gui style="button">Κλείσιμο</gui>.</p>

</page>
